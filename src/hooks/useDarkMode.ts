// custom hook for dark mode functionality
import { useState, useEffect } from 'react'

const useDarkMode = () => {
    const [theme, setTheme] = useState('light')

    const toggleTheme = () => {
        const newTheme = theme === 'light' ? 'dark' : 'light'
        setTheme(newTheme)
        localStorage.setItem('theme', newTheme)
    }

    useEffect(() => {
        const localTheme = localStorage.getItem('theme')
        if (localTheme) {
            setTheme(localTheme)
        }
    }, [])

    return {
        theme,
        toggleTheme,
    }
}

export default useDarkMode
