import React from 'react'
/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react'
import { useMarkdownContext } from '../contexts/MarkdownContext'

const Footer: React.FC = () => {
    const { theme } = useMarkdownContext()
    return (
        <div
            css={css`
                padding: 1rem 0;
                flex-grow: 1;
                text-align: center;
                background-color: ${theme === 'dark' ? '#000' : '#eeeeee'};
                color: ${theme === 'dark' ? '#fff' : '#000'};
                border-top: ${theme === 'dark' ? 'none' : '2px solid #aaa'};
                position: sticky;
                bottom: 0;
            `}
        >
            Markdown Editor ©️, By Similoluwa Okunowo ❤️️
        </div>
    )
}

export default Footer
