import React, { useEffect, useRef, useState } from 'react'

/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react'
import { useMarkdownContext } from '../contexts/MarkdownContext'
import { Resizable } from 're-resizable'

const Editor: React.FC = () => {
    const { markdownContent, updateMarkdownContent, theme } = useMarkdownContext()
    const editorElementRef = useRef<HTMLTextAreaElement>(null)

    useEffect(() => {
        const loadExampleMarkdown = async () => {
            fetch('./examples/demo.md')
                .then((r) => r.text())
                .then((resp) => {
                    if (editorElementRef.current) {
                        editorElementRef.current.value = resp
                        updateMarkdownContent(resp)
                    }
                })
        }
        if (!markdownContent) {
            loadExampleMarkdown()
        }
    }, [])

    return (
        <div
            css={css`
                height: 80vh;
                display: flex;
                flex: 1;
                flex-direction: column;
                padding: 1rem 0;
                background-color: ${theme === 'dark' ? '#333336' : '#eee'};
            `}
        >
            <textarea
                ref={editorElementRef}
                className='editor'
                onChange={(e) => {
                    console.log(e.target.value)
                    updateMarkdownContent(e.target.value)
                }}
                css={css`
                    width: 100%;
                background-color: ${theme === 'dark' ? '#333336' : '#eee'};
                color: ${theme === 'dark' ? '#fff' : '#000'};
                padding: .5rem 1rem;
                border: none;
                outline: none; 
                box-shadow: none;
                font-size: 16px;
                word-wrap: break-word;
                overflow-y:scroll;
                height: 100%;
            }
        `}
            />
        </div>
    )
}

const ResizableEditor: React.FC = () => {
    const [editorWidth, setEditorWidth] = useState<number | null>(null)
    // get the window width
    useEffect(() => {
        const handleScreenResize = () => {
            console.log('handling screen size')
            setEditorWidth(Math.round(window.innerWidth / 2))
        }

        window.addEventListener('resize', handleScreenResize)

        handleScreenResize()

        return () => window.removeEventListener('resize', handleScreenResize)
    }, [])

    if (!editorWidth) {
        return <div></div>
    }
    return (
        <Resizable
            size={{ width: editorWidth, height: 700 }}
            onResizeStop={(e, direction, ref, d) => {
                setEditorWidth((prev) => Number(prev) + d.width)
            }}
        >
            <Editor />
        </Resizable>
    )
}
export default ResizableEditor
