import React from 'react'
import ReactMarkdown from 'react-markdown'
import { Components } from 'react-markdown/lib/ast-to-react'
import { materialLight } from 'react-syntax-highlighter/dist/cjs/styles/prism'
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import remarkGfm from 'remark-gfm'
import remarkMath from 'remark-math'
import rehypeKatex from 'rehype-katex'
import 'katex/dist/katex.min.css'

import '../styles/Markdown.css'

/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react'
import { useMarkdownContext } from '../contexts/MarkdownContext'

export const toMarkdown = (content: string) => {
    const components: Components = {
        code({ node, inline, className, children, ...props }) {
            const match = /language-(\w+)/.exec(className || '')
            return !inline && match ? (
                <SyntaxHighlighter
                    style={materialLight}
                    language={match[1]}
                    PreTag='div'
                    {...props}
                >
                    {String(children).replace(/\n$/, '')}
                </SyntaxHighlighter>
            ) : (
                <code className={className} {...props}>
                    {children}
                </code>
            )
        },
    }

    return (
        <ReactMarkdown
            components={components}
            remarkPlugins={[remarkGfm, remarkMath]}
            rehypePlugins={[rehypeKatex]}
        >
            {content}
        </ReactMarkdown>
    )
}

const Preview: React.FC = () => {
    const { markdownContent, theme } = useMarkdownContext()

    return (
        <div
            css={css`
                display: flex;
                flex-direction: column;
                flex: 1;
                background-color: ${theme === 'dark' ? '#555559' : '#fff'};
                color: ${theme === 'dark' ? '#fff' : '#000'};
                padding: 0.5rem 1rem;
                overflow-y: scroll;
                overflow-x: hidden;
                padding: 0.5rem 1rem;
                word-wrap: break-word;
                height: 80vh;
            `}
        >
            <div className='preview'>{toMarkdown(markdownContent)}</div>
        </div>
    )
}

export default Preview
