import styled from '@emotion/styled'

export const ColumnFlex = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`

export const RowFlex = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`
