import React from 'react'
import { css } from '@emotion/react'
import Preview from './Preview'
import Editor from './Editor'
import { useMarkdownContext } from '../contexts/MarkdownContext'

/** @jsxImportSource @emotion/react */

const Main: React.FC = () => {
    const { theme } = useMarkdownContext()
    return (
        <div
            css={css`
                display: flex;
                flex-direction: row;
                flex: 1 auto;
                flex-wrap: wrap;
                width: 100%;
                justify-content: space-between;
                color: ${theme === 'dark' ? '#fff' : '#000'};
                background-color: ${theme === 'dark' ? '#222324' : '#eeeeee'};
                position: relative;
            `}
        >
            <Editor />
            <Preview />
        </div>
    )
}

export default Main
