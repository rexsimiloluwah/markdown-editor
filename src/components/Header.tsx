import React from 'react'
import { FiSun, FiCopy } from 'react-icons/fi'
import { FaMoon } from 'react-icons/fa'
/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react'
import { VscFilePdf } from 'react-icons/vsc'
import { BsMarkdown } from 'react-icons/bs'
import ReactTooltip from 'react-tooltip'
import { useMarkdownContext } from '../contexts/MarkdownContext'
// @ts-ignore
import html2pdf from 'html2pdf.js'

const Header: React.FC = () => {
    const { markdownContent, theme, toggleTheme } = useMarkdownContext()

    // Download PDF
    const handleDownloadPdf = () => {
        console.log(document.querySelector('.preview'))
        const opt = {
            margin: 1,
            filename: `out-file-${new Date().getTime().toString()}.pdf`,
            image: { type: 'jpeg', quality: 0.98 },
            html2canvas: { scale: 2 },
            jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' },
        }
        html2pdf().set(opt).from(document.querySelector('.preview')).save()
    }

    // Download Markdown
    const handleDownloadMd = () => {
        const element = document.createElement('a')
        const file = new Blob([markdownContent], {
            type: 'text/markdown',
        })
        element.href = URL.createObjectURL(file)
        element.download = `out-file-${new Date().getTime().toString()}.md`
        document.body.appendChild(element)
        element.click()
    }

    // Copy to clipboard
    const copyToClipboard = () => {
        navigator.clipboard.writeText(markdownContent)
        alert('Copy successful!')
    }

    const headerButtonDefaultStyle = css`
        outline: none;
        padding: 0.5rem;
        color: #000;
        box-shadow: none;
        cursor: pointer;
        border-radius: 5px;
        background: transparent;
    `

    return (
        <header
            css={css`
                display: flex;
                flex-direction: row;
                flex-grow: 1;
                justify-content: space-between;
                padding: 1.2rem 3rem;
                align-items: center;
                font-size: 24px;
                height: 100%;
                background-color: ${theme === 'dark' ? '#000' : '#eeeeee'};
                color: ${theme === 'dark' ? '#fff' : '#000'};
                box-shadow: 7px 11px 9px -5px rgba(0, 0, 0, 0.41);
                border-bottom: ${theme === 'dark' ? 'none' : '2px solid #aaa'};
            `}
        >
            <div>Markdown Editor</div>

            <div
                css={css`
                    display: flex;
                    align-items: center;
                    gap: 5%;
                `}
            >
                <ReactTooltip id='downloadPdf' place='bottom' effect='solid'>
                    Download PDF
                </ReactTooltip>
                <ReactTooltip id='downloadMd' place='bottom' effect='solid'>
                    Download .md
                </ReactTooltip>
                <ReactTooltip id='copyMd' place='bottom' effect='solid'>
                    Copy to clipboard
                </ReactTooltip>
                <button
                    data-tip
                    data-for='downloadPdf'
                    type='button'
                    onClick={() => {
                        handleDownloadPdf()
                    }}
                    css={css`
                        ${headerButtonDefaultStyle}
                        color: crimson;
                        border: 1px solid crimson;
                        &:hover {
                            background: crimson;
                            color: white;
                        }
                    `}
                >
                    <VscFilePdf size={24} />
                </button>
                <button
                    data-tip
                    data-for='downloadMd'
                    type='button'
                    onClick={() => {
                        handleDownloadMd()
                    }}
                    css={css`
                        ${headerButtonDefaultStyle}
                        color: ${theme === 'dark' ? '#fff' : '#000'};
                        border: 1px solid ${theme === 'dark' ? '#fff' : '#000'};
                        &:hover {
                            background: #000;
                            border-color: ${theme === 'dark' ? '#fff' : '#000'};
                            color: #fff;
                        }
                    `}
                >
                    <BsMarkdown size={24} />
                </button>
                <button
                    data-tip
                    data-for='copyMd'
                    type='button'
                    onClick={() => {
                        copyToClipboard()
                    }}
                    css={css`
                        ${headerButtonDefaultStyle}
                        color: dodgerblue;
                        border: 1px solid dodgerblue;
                        &:hover {
                            background: dodgerblue;
                            color: #fff;
                        }
                    `}
                >
                    <FiCopy size={24} />
                </button>
                <div
                    onClick={() => toggleTheme()}
                    css={css`
                        cursor: pointer;
                        font-size: 24px;
                        padding: 0 2rem;
                    `}
                >
                    {theme === 'dark' ? <FiSun /> : <FaMoon />}
                </div>
            </div>
        </header>
    )
}

export default Header
