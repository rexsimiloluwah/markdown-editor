import React, { createContext, useState, useContext } from 'react'
import useDarkMode from '../hooks/useDarkMode'

export interface IMarkdownContext {
    markdownContent: string
    updateMarkdownContent: (value: string) => void
    theme: string
    toggleTheme: () => void
}

export const MarkdownContext = createContext({} as IMarkdownContext)

export const MarkdownProvider: React.FC = ({ children }) => {
    const [markdownContent, setMarkdownContent] = useState<string>('')
    const { theme, toggleTheme } = useDarkMode()

    const updateMarkdownContent = (value: string) => {
        setMarkdownContent(value)
    }

    return (
        <MarkdownContext.Provider
            value={{
                markdownContent,
                updateMarkdownContent,
                theme,
                toggleTheme,
            }}
        >
            {children}
        </MarkdownContext.Provider>
    )
}

export const useMarkdownContext = () => useContext(MarkdownContext)
