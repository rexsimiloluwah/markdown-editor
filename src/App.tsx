import React from 'react'
import Header from './components/Header'
import Footer from './components/Footer'
import Main from './components/Main'
import { MarkdownProvider } from './contexts/MarkdownContext'
import './index.css'

// @jsxImportSource @emotion/react

function App() {
    return (
        <div className='App'>
            <MarkdownProvider>
                <Header />
                <Main />
                <Footer />
            </MarkdownProvider>
        </div>
    )
}

export default App
