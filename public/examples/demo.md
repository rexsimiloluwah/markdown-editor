# This is a Heading 1

## This is a Heading 2

### This is a Heading 3

#### This is a Heading 4

##### This is a Heading 5

###### This is a Heading 6

**This is a Bold Format**
_This is an Italicized Format_
~This is a Strike-Through Format~

### Lists

-   List Item 1
-   List Item 2
-   List Item 3
-   [x] Checked
-   [ ] Unchecked

### LaTex (Math)

Einstein's Equation is: $E = M*C^2$

Schrodinger's Time-Independent Wave Equation: $\frac{-\hbar^2}{2m}\frac{\partial^2 \psi}{\partial x^2} + V.\psi = E.\psi$

### Syntax Highlighting

```ts
function addNumbers(a: number, b: number): number {
    return a + b
}
```

### Images

![Yes](https://i.imgur.com/sZlktY7.png)
